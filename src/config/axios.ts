import axios from 'axios';

export default axios.create({
	baseURL: 'http://demo6535427.mockable.io',
	timeout: 10000,
});
