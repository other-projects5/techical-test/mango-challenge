import HttpClient from '../config/axios';

import { Range } from '../models';

export class RangeService {
	static async getBasicRangeValues(): Promise<Range> {
		const endpoint = '/basic-range';

		try {
			const response = await HttpClient.get(endpoint);
			return response.data.range;
		} catch (error) {
			console.error('Error', error);
			return { min: 1, max: 100 };
		}
	}

	static async getStepRangeValues(): Promise<number[]> {
		const endpoint = '/step-range';

		try {
			const response = await HttpClient.get(endpoint);
			return response.data.steps;
		} catch (error) {
			console.error('Error', error);
			return [0, 10, 20, 30, 40, 50, 60];
		}
	}
}
