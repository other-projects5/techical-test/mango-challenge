export interface Range {
	min: number;
	max: number;
}

export type RangeType = 'slide' | 'step';
