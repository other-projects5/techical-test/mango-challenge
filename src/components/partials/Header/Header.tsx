import * as React from 'react';
import { FunctionComponent } from 'react';

import { Container } from '../../../__lib__/react-components';

import styles from './Header.module.scss';

export const Header: FunctionComponent = () => {
	return (
		<div className={styles.wrapper}>
			<Container className={styles.headerContent}>
				<div className={styles.title}>Custom Range input</div>
			</Container>
		</div>
	);
};
