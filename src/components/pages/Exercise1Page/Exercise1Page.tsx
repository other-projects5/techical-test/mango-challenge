import * as React from 'react';
import { FunctionComponent, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { Container } from '../../../__lib__/react-components';

import { RangeService } from '../../../services';
import { Range } from '../../../models';
import { routes } from '../../routes';
import { Button, InputRange } from '../../widgets';

export const Exercise1Page: FunctionComponent = () => {
	const [rangeValues, setRangeValues] = useState<undefined | Range>(
		undefined
	);

	useEffect(() => {
		RangeService.getBasicRangeValues().then((res: Range) => {
			setRangeValues(res);
		});
	}, []);

	return (
		<Container
			style={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				flexDirection: 'column',
			}}
		>
			<h2>Double input range</h2>
			<div>
				<Link to={routes.home}>
					<Button rounded outlined text="Home" />
				</Link>
			</div>
			<div style={{ marginTop: '50px' }}>
				{rangeValues && <InputRange limit={rangeValues as Range} />}
			</div>
		</Container>
	);
};
