import * as React from 'react';
import { FunctionComponent, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { routes } from '../../routes';
import { RangeService } from '../../../services';
import { Container } from '../../../__lib__/react-components';
import { Button, InputRange } from '../../widgets';

export const Exercise2Page: FunctionComponent = () => {
	const [rangeValues, setRangeValues] = useState<undefined | number[]>(
		undefined
	);

	useEffect(() => {
		RangeService.getStepRangeValues().then((res: number[]) => {
			console.log('res', res);
			setRangeValues(res);
		});
	}, []);

	return (
		<Container
			style={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				flexDirection: 'column',
			}}
		>
			<h2>Stepped input range</h2>
			<div>
				<Link to={routes.home}>
					<Button rounded outlined text="Home" />
				</Link>
			</div>
			<div style={{ marginTop: '50px' }}>
				{rangeValues && (
					<InputRange
						rangeType="step"
						steps={rangeValues as number[]}
					/>
				)}
			</div>

			<div
				style={{
					marginTop: '100px',
					padding: '20px',
					backgroundColor: 'lightgray',
					borderRadius: 8,
				}}
			>
				<p>
					Esta parte solo funciona en una direccion. No he podido
					dedicarle mucho mas tiempo.
				</p>
				<p>
					LeftBullet se mueve hacia la derecha respetando la distancia
					con el RightBullet
				</p>
				<p>
					RightBullet se mueve hacia la izquierda respetando la
					distancia con el LeftBullet
				</p>
			</div>
		</Container>
	);
};
