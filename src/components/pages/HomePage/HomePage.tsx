import * as React from 'react';
import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';

import { Container } from '../../../__lib__/react-components';

import { routes } from '../../routes';
import { Section } from '../../templates';
import { Button } from '../../widgets';

import styles from './HomePage.module.scss';

export const HomePage: FunctionComponent = () => {
	return (
		<Container>
			<Section title="Double Bullet InputRange">
				<Link to={routes.exercise1} className={styles.link}>
					<Button rounded text="Try it!" />
				</Link>
			</Section>
			<Section title="Stepped Bullet InputRange">
				<Link to={routes.exercise2} className={styles.link}>
					<Button rounded text="Try it!" />
				</Link>
			</Section>
		</Container>
	);
};
