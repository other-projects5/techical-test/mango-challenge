import * as React from 'react';
import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';

import { routes } from '../../routes';

export const NotFoundPage: FunctionComponent = () => {
	return (
		<div>
			Component NOT FOUND
			<div>
				Go - <Link to={routes.home}>Home Page</Link>
			</div>
		</div>
	);
};
