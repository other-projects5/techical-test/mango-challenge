import * as React from 'react';
import { FunctionComponent } from 'react';

import { Switch, Route } from 'react-router-dom';

import { HomePage, Exercise1Page, Exercise2Page, NotFoundPage } from '../pages';
import { routes } from './routes';

export const Routes: FunctionComponent = () => {
	const { home, exercise1, exercise2 } = routes;

	return (
		<Switch>
			<Route exact path={home} component={HomePage} />
			<Route exact path={exercise1} component={Exercise1Page} />
			<Route exact path={exercise2} component={Exercise2Page} />

			<Route path="*" component={NotFoundPage} />
		</Switch>
	);
};
