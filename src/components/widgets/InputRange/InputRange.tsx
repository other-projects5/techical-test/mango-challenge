import * as React from 'react';
import {
	FunctionComponent,
	useState,
	useEffect,
	CSSProperties,
	useRef,
	ChangeEvent,
} from 'react';

import { Range, RangeType } from '../../../models';

import styles from './InputRange.module.scss';

interface RangeProps {
	rangeType?: RangeType;
	steps?: number[];
	limit?: Range;
}

export const InputRange: FunctionComponent<RangeProps> = (props) => {
	const { rangeType = 'slide', steps, limit } = props;
	const [min, setMin] = useState<number>(0);
	const [max, setMax] = useState<number>(0);

	const [stepsPercent, setStepsPercent] = useState<number[]>([]);

	const [leftBullet, setLeftBullet] = useState<number>();
	const leftRef = useRef<HTMLDivElement>(null);
	const [rightBullet, setRightBullet] = useState<number>();
	const rightRef = useRef<HTMLDivElement>(null);

	const [minProgress, setMinProgress] = useState<number>(0);
	const [maxProgress, setMaxProgress] = useState<number>(100);

	const barSizeHeight = 10;
	const bulletSize = 20;
	const bulletStyle: CSSProperties = {
		width: bulletSize,
		height: bulletSize,
		top: (barSizeHeight - bulletSize) / 2,
	};

	useEffect(() => {
		const bulletPercentage = calcBulletPercentage(bulletSize);
		setLeftBullet(0 - bulletPercentage / 2);
		setRightBullet(100 - bulletPercentage / 2);

		if (rangeType === 'slide' && limit) {
			setMin(limit.min);
			setMax(limit.max);
		} else if (rangeType === 'step' && steps && steps.length > 0) {
			const stepsAmount = steps?.length - 1;
			const percentBetween = 100 / stepsAmount;

			setMin(steps[0]);
			setMax(steps[stepsAmount]);
			setStepsPercent(
				Array.from(Array(steps?.length)).map(
					(e, i) => percentBetween * i
				)
			);
		}
	}, [limit, rangeType, steps]);

	useEffect(() => {
		let bulletClicked = false;

		const htmlLeft = document.getElementById('left-bullet');
		htmlLeft?.addEventListener('mouseup', () => {
			bulletClicked = false;
			scaleBulletOnClick(htmlLeft);
		});
		htmlLeft?.addEventListener('mousedown', () => {
			bulletClicked = true;
			scaleBulletOnClick(htmlLeft, true);
		});
		htmlLeft?.addEventListener('mousemove', (event) =>
			onMouseMove(event, bulletClicked, 'left')
		);

		const htmlRight = document.getElementById('right-bullet');
		htmlRight?.addEventListener('mouseup', () => {
			bulletClicked = false;
			scaleBulletOnClick(htmlRight);
		});
		htmlRight?.addEventListener('mousedown', () => {
			bulletClicked = true;
			scaleBulletOnClick(htmlRight, true);
		});
		htmlRight?.addEventListener('mousemove', (event) =>
			onMouseMove(event, bulletClicked, 'right')
		);

		return () => {
			htmlLeft?.addEventListener('mouseup', () => {
				bulletClicked = false;
			});
			htmlLeft?.addEventListener('mousedown', () => {
				bulletClicked = true;
			});
			htmlLeft?.removeEventListener('mousemove', (event) =>
				onMouseMove(event, bulletClicked, 'left')
			);
			htmlRight?.addEventListener('mouseup', () => {
				bulletClicked = false;
			});
			htmlRight?.addEventListener('mousedown', () => {
				bulletClicked = true;
			});
			htmlRight?.removeEventListener('mousemove', (event) =>
				onMouseMove(event, bulletClicked, 'right')
			);
		};
	});

	const calcBulletPercentage = (bulletSize: number): number => {
		const bar = (
			document.getElementById('bar') as HTMLElement
		).getBoundingClientRect();
		return (bulletSize * 100) / bar.width;
	};

	const scaleBulletOnClick = (element: HTMLElement, scale = false): void => {
		const size = bulletSize + (scale ? 10 : 0);
		element.style.width = `${size}px`;
		element.style.height = `${size}px`;
		element.style.top = `${(barSizeHeight - size) / 2}px`;
		element.style.cursor = scale ? 'grabbing' : 'grab';
	};

	const onMouseMove = (
		event: MouseEvent,
		clicked: boolean,
		type: 'left' | 'right'
	): void => {
		if (clicked) {
			const bar = document.getElementById('bar') as HTMLElement;
			const barDimensions = bar.getBoundingClientRect();
			const dimensions = {
				width: bar.clientWidth,
				startX: barDimensions.left,
				endX: barDimensions.right,
			};
			const clickedBullet = (bulletSize + 10) / 2;
			const clickedBulletPercent = calcBulletPercentage(bulletSize + 10);

			const currentBulletPosition = parseFloat(
				Math.abs(
					((dimensions.startX - event.clientX + clickedBullet) *
						100) /
						dimensions.width
				).toString()
			);

			const checkBoundaries =
				event.clientX - clickedBullet >= dimensions.startX &&
				event.clientX - clickedBullet <= dimensions.endX;

			if (type === 'left' && checkBoundaries && rightRef.current) {
				const rightPos = parseFloat(
					rightRef.current.style.left.slice(
						0,
						rightRef.current.style.left.length - 1
					)
				);
				if (currentBulletPosition < rightPos - clickedBulletPercent) {
					if (rangeType === 'slide') {
						setLeftBullet(
							currentBulletPosition - clickedBulletPercent / 10
						);
						setMinProgress(currentBulletPosition);
						setMin((currentBulletPosition * limit!.max) / 100);
					} else {
						const aux =
							currentBulletPosition - clickedBulletPercent / 10;
						const nextStep = stepsPercent.find(
							(item) => aux < item - clickedBulletPercent / 10
						)!;

						if (nextStep && nextStep < rightPos) {
							setLeftBullet(nextStep - clickedBulletPercent / 10);
							setMinProgress(nextStep);
							setMin(
								steps![
									stepsPercent.findIndex(
										(item) => item === nextStep
									)
								]
							);
						}
					}
				}
			} else if (type === 'right' && checkBoundaries && leftRef.current) {
				const leftPos = parseFloat(
					leftRef.current.style.left.slice(
						0,
						leftRef.current.style.left.length - 1
					)
				);
				if (currentBulletPosition > leftPos + clickedBulletPercent) {
					if (rangeType === 'slide') {
						setRightBullet(
							currentBulletPosition - clickedBulletPercent / 10
						);
						setMaxProgress(currentBulletPosition);
						setMax((currentBulletPosition * limit!.max) / 100);
					} else {
						const aux =
							currentBulletPosition - clickedBulletPercent / 10;
						const nextStep = stepsPercent.find(
							(item, index, array) => aux < array[index + 1]
						)!;

						if (nextStep && nextStep > leftPos) {
							setRightBullet(
								nextStep - clickedBulletPercent / 10
							);
							setMaxProgress(nextStep);
							setMax(
								steps![
									stepsPercent.findIndex(
										(item) => item === nextStep
									)
								]
							);
						}
					}
				}
			}
		}
	};

	const onInputChange = (
		event: ChangeEvent<HTMLInputElement>,
		type: 'left' | 'right'
	): void => {
		const value = parseInt(event.target.value);
		const percentage = (value * 100) / limit!.max;

		if (type === 'left' && value < max! && value >= limit!.min) {
			setMin(value);
			setLeftBullet(percentage);
			setMinProgress(percentage);
		} else if (type === 'right' && value > min! && value <= limit!.max) {
			setMax(value);
			setRightBullet(percentage);
			setMaxProgress(percentage);
		}
	};

	return (
		<div data-test="input-range" className={styles.wrapper}>
			<div>
				<label className={styles.label}>min:</label>
				<input
					data-test="left-input"
					type="number"
					className={styles.input}
					value={min}
					disabled={rangeType === 'step'}
					onChange={(event) => onInputChange(event, 'left')}
				/>
			</div>

			<div className={styles.rangeWrapper}>
				<div
					data-test="left-bullet"
					id="left-bullet"
					ref={leftRef}
					className={styles.bullet}
					style={{ ...bulletStyle, left: `${leftBullet}%` }}
				/>
				<div id="bar" className={styles.bar}>
					<div
						data-test="progress-bar"
						className={styles.progress}
						style={{
							left: `${minProgress}%`,
							right: `${100 - maxProgress}%`,
						}}
					/>

					{steps && steps.length > 0 && (
						<div className={styles.steppedBar}>
							{steps.map((step) => (
								<div key={step}>{step}</div>
							))}
						</div>
					)}
				</div>
				<div
					data-test="right-bullet"
					id="right-bullet"
					ref={rightRef}
					className={styles.bullet}
					style={{ ...bulletStyle, left: `${rightBullet}%` }}
				/>
			</div>

			<div>
				<label className={styles.label}>max:</label>
				<input
					data-test="right-input"
					type="number"
					className={styles.input}
					value={max}
					disabled={rangeType === 'step'}
					onChange={(event) => onInputChange(event, 'right')}
				/>
			</div>
		</div>
	);
};
