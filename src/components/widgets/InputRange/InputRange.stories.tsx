import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { InputRange } from './InputRange';

export default {
	title: 'Widgets/InputRange',
	component: InputRange,
} as ComponentMeta<typeof InputRange>;

const Template: ComponentStory<typeof InputRange> = (args) => (
	<InputRange {...args} />
);

export const SlideRange = Template.bind({});
SlideRange.args = {
	rangeType: 'slide',
	limit: { min: 1, max: 1000 },
};

export const StepRange = Template.bind({});
StepRange.args = {
	rangeType: 'step',
	steps: [0, 10, 20, 30, 40, 50, 60],
};
