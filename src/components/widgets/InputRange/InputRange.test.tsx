import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import { Range } from '../../../models';
import { InputRange } from './InputRange';

configure({ adapter: new Adapter() });

describe('InputRange', () => {
	const limit: Range = {
		min: 1,
		max: 1000,
	};

	test('Should be rendered', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		expect(wrapper.find({ 'data-test': 'input-range' }).length).toBe(1);
	});

	test('Change left input', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		wrapper
			.find({ 'data-test': 'left-input' })
			.simulate('change', { target: { value: limit.max / 2 } });

		expect(wrapper.find({ 'data-test': 'left-input' }).props().value).toBe(
			limit.max / 2
		);
	});

	test('Change right input', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		wrapper
			.find({ 'data-test': 'right-input' })
			.simulate('change', { target: { value: limit.max / 2 } });

		expect(wrapper.find({ 'data-test': 'right-input' }).props().value).toBe(
			limit.max / 2
		);
	});

	test('Left input lower than right input', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		wrapper
			.find({ 'data-test': 'right-input' })
			.simulate('change', { target: { value: 100 } });
		wrapper
			.find({ 'data-test': 'left-input' })
			.simulate('change', { target: { value: 110 } });

		expect(
			wrapper.find({ 'data-test': 'left-input' }).props().value
		).toBeLessThan(100);
	});

	test('Right input higher than left input', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		wrapper
			.find({ 'data-test': 'left-input' })
			.simulate('change', { target: { value: 110 } });
		wrapper
			.find({ 'data-test': 'right-input' })
			.simulate('change', { target: { value: 100 } });

		expect(
			wrapper.find({ 'data-test': 'right-input' }).props().value
		).toBeGreaterThan(110);
	});

	test('Left input moves left bullet', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		wrapper
			.find({ 'data-test': 'left-input' })
			.simulate('change', { target: { value: limit.max / 2 } });

		expect(wrapper.find({ 'data-test': 'left-input' }).props().value).toBe(
			limit.max / 2
		);
		expect(
			wrapper.find({ 'data-test': 'left-bullet' }).props().style.left
		).toBe('50%');
	});

	test('Right input moves right bullet', () => {
		const wrapper = shallow(<InputRange limit={limit} />);

		wrapper
			.find({ 'data-test': 'right-input' })
			.simulate('change', { target: { value: limit.max / 2 } });

		expect(wrapper.find({ 'data-test': 'right-input' }).props().value).toBe(
			limit.max / 2
		);
		expect(
			wrapper.find({ 'data-test': 'right-bullet' }).props().style.left
		).toBe('50%');
	});
});
